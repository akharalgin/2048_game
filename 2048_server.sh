#!/bin/bash

cd /opt/
sudo apt install nginx npm -y
sudo git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git
cd ./2048-game
sudo npm install --include=dev
sudo npm run build
ip=$(curl ifconfig.me)
sudo tee > /etc/nginx/sites-available/default << EOL
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        server_name $ip;
        location / {
                proxy_pass http://localhost:8080;
        }
} 
EOL

sudo nginx -s reload
sudo touch /etc/systemd/system/2048.service
sudo tee > /etc/systemd/system/2048.service << EOL
[Unit]
Description=2048 Game
After=syslog.target
After=network.target
After=nginx.target

[Service]
Type=simple
WorkingDirectory=/opt/2048-game
User=root
Group=root
OOMScoreAdjust=-100
ExecStart=/usr/bin/npm start

[Install]
WantedBy=multi-user.target
EOL

sudo systemctl enable 2048.service
sudo systemctl start 2048.service